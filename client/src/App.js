import React, { Component } from 'react'
import Calculator from './components/calculator'
import 'bootstrap/dist/css/bootstrap.min.css'

class App extends Component {
  render() {
    return (
      <div className="App">      
        <Calculator />
      </div>
    )
  }
}

export default App
