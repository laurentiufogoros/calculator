import React from 'react'
import Button from './button'
import ClientFetch from '../utils/ClientFetch'


const emptyObject = { 
        input: '',       
        leftTerm: '',
        rightTerm: '',
        operation:'' 
      }

class Calculator extends React.Component {
    constructor() {
      super()
      this.state = emptyObject
      this.handleClick = this.handleClick.bind(this)
    }

    getAPIPath()
    {		
      switch(this.state.operation.trim())
      {
        case "+":
          return "api_get_add_result"			
        case "-":
          return "api_get_sub_result"
        case "*":
          return "api_get_mul_result"
        case "/":
          return "api_get_div_result"
        default:
          return ""
      }
    }

    calculate(){
      const clientFetch = new ClientFetch()
      clientFetch.fetch(this.getAPIPath() + '?a=' + this.state.leftTerm + '&b=' + this.state.rightTerm, {method: "GET"})
        .then(res => {
          this.setState({ input: res.result.toString() , leftTerm: res.result, operation:'', rightTerm:''})   
        })
    }

    handleClick(event){  
      const value = event.target.value
      event.preventDefault()

      switch (value) {
        case '=': { 
          this.calculate()
          break
        }
        case 'C': {          
          this.setState(emptyObject)
          break
        }
        case '*':
        case '+':
        case '-':
        case '/': { 
          
          if (this.state.operation === '' || this.state.operation !== value)                 
            this.setState({ input: this.state.leftTerm + value, operation: ` ${value} ` })
          break
        }
        default: {
          if (this.state.operation !== '') {
            this.setState({ rightTerm: this.state.rightTerm + value})
          }
          else{
            this.setState({ leftTerm: this.state.leftTerm + value})
          }
      
          this.setState({ input: this.state.input + value})
          break
        }      
      }      
    }

    render() {
      return (      
        
          <div className="frame">          
            <div>
              <input className={'input-screen'} type="text" readOnly value={this.state.input}/>
            </div>
            <div className="btn-row">
              <Button className='btn-primary' label={'1'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'2'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'3'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'4'} handleClick={this.handleClick} />
              <Button className='btn-info' label={'-'} handleClick={this.handleClick} />
              <Button className='btn-info' label={'+'} handleClick={this.handleClick} />
            </div>
            <div className="btn-row">
              <Button className='btn-primary' label={'5'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'6'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'7'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'8'} handleClick={this.handleClick} />
              <Button className='btn-info' label={'*'} handleClick={this.handleClick} />
              <Button className='btn-info' label={'/'} handleClick={this.handleClick} />
            </div>
            <div className="btn-row">
              <Button className='btn-primary' label={'9'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'.'} handleClick={this.handleClick} />
              <Button className='btn-primary' label={'0'} handleClick={this.handleClick} />
              <Button className='btn-warning' label={'C'} handleClick={this.handleClick} />
              <Button className='btn-success'label={'='} handleClick={this.handleClick} />
            </div>
          </div>
      )
    }
}

export default Calculator
