import React from 'react'
import PropTypes from 'prop-types'


const Button = (props) => {
  return (
    <button     
      className={`${props.className} i-btn`}
      onClick={props.handleClick}
      value={props.label}
    >{props.label}
    </button>
  )
}


Button.propTypes = {  
  handleClick: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string
}

export default Button
