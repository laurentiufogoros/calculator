class ClientFetch {


    constructor(domain) {
        if(ClientFetch.instance !== null)
        {
            return ClientFetch.instance
        }   
        this.domain = domain || 'http://localhost:3000' 
        this.fetch = this.fetch.bind(this) 
        
        ClientFetch.instance = this
    }


    fetch(url, options, isMultipart = false) {

        const headers = {
            'Accept': 'application/json'
        }
        if(!isMultipart)
        {
            headers['Content-Type'] = 'application/json'
        }

        return fetch(url, {
            headers,
            ...options
        })
        .then(this._checkStatus)
        .then(response => response.json())
    }



    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) { 
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}

Object.defineProperty(ClientFetch, 'instance', {
    value: null,
    writable : true,
    enumerable : true,
    configurable : false
})

ClientFetch.getInstance = function(domain)
{
    if(ClientFetch.instance !== null)
    {
        return ClientFetch.instance
    }
    else
    {
        return new ClientFetch(domain)
    }
}

export default ClientFetch