# React + Node.js Calculator #

### What is this repository for? ###

* React calculator with UI and Backend in separate folders and using separate http servers
* Version 1.0.0

### How do I get set up? ###

* Clone or download the repository
* There are 2 folders client and server 
	* in client folder execute npm start, default http port for Client is 3000
	* in server folder and execute npm start, default http port for Client is 3100
* To run tests for client go in client folder and execute npm run test  - this tests are only a POC
* To run tests for server go in server folder and execute npm run test  - this tests are full and with coverage 100%

