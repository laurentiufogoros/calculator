module.exports.calcAdd = function (req, res) {
	const validate = validateValues(req, res)
	console.log("validate: " + validate);
	if (validate)
		returnResponse(validate.a + validate.b, res)
}


module.exports.calcSub = function (req, res) {
	const validate = validateValues(req, res)
	if (validate)
		returnResponse(validate.a - validate.b, res)
}

module.exports.calcMul = function (req, res) {	
	const validate = validateValues(req, res)
	if (validate)
		returnResponse(validate.a * validate.b, res)
}

module.exports.calcDiv = function (req, res) {
	const validate = validateValues(req, res)	
	if (validate)
		returnResponse(validate.a / validate.b, res)	
}

validateValues = function (req, res){	
	const a = req.query.a
	const b = req.query.b
	
	if(isNaN(a + '') || isNaN(b + ''))
	{	
		res.type('json')
		res.send(JSON.stringify("Not a number"))
		return
	}

	return {a: parseFloat(a), b: parseFloat(b)}
}

returnResponse = function (result, res) {
	res.type('json')
	res.send(JSON.stringify({
		result: result
	}))
}