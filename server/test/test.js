
const app = require("../app");
const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = chai;
chai.use(chaiHttp);


describe("Server!", () => {
  it("add 2 numbers", done => {
    chai.request(app)
      .get("/api_get_add_result?a=5&b=5")
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(200);        
        expect(res.body.result).to.equals(10);
        done();
      });
  });

  it("sub 2 numbers", done => {
    chai.request(app)
      .get("/api_get_sub_result?a=5&b=5")
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(200);        
        expect(res.body.result).to.equals(0);
        done();
      });
  });

  it("multiply 2 numbers", done => {
    chai.request(app)
      .get("/api_get_mul_result?a=5&b=5")
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(200);        
        expect(res.body.result).to.equals(25);
        done();
      });
  });

  it("div 2 numbers", done => {
    chai.request(app)
      .get("/api_get_div_result?a=5&b=5")
      .end((err, res) => {
        expect(res).to.have.status(200);        
        expect(res.body.result).to.equals(1);
        done();
      });
  });

  it("add 2 wrong numbers", done => {
    chai.request(app)
      .get("/api_get_add_result?a=a&b=b")
      .end((err, res) => {
        expect(res).to.have.status(200);  
        expect(res.body).to.equals("Not a number");
        done();
      });
  });

  it("sub 2 numbers", done => {
    chai.request(app)
      .get("/api_get_sub_result?a=a&b=b")
      .end((err, res) => {
        expect(res).to.have.status(200);  
        expect(res.body).to.equals("Not a number");
        done();
      });
  });

  it("multiply 2 wrong numbers", done => {
    chai.request(app)
      .get("/api_get_mul_result?a=a&b=b")
      .end((err, res) => {
        expect(res).to.have.status(200);  
        expect(res.body).to.equals("Not a number");
        done();
      });
  });

  it("div 2 wrong numbers", done => {
    chai.request(app)
      .get("/api_get_div_result?a=a&b=b")
      .end((err, res) => {
        expect(res).to.have.status(200);  
        expect(res.body).to.equals("Not a number");
        done();
      });
  });
});
